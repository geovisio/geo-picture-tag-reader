import pytest
import datetime
from typing import Optional
from geopic_tag_reader.sequence import (
    SortMethod,
    MergeParams,
    SplitParams,
    SplitReason,
    Sequence,
    Picture,
    sort_pictures,
    find_duplicates,
    split_in_sequences,
    dispatch_pictures,
)
from geopic_tag_reader.reader import GeoPicTags


def test_MergeParams_is_merge_needed():
    mp = MergeParams()
    assert not mp.is_merge_needed()
    mp = MergeParams(maxDistance=1)
    assert mp.is_merge_needed()
    mp = MergeParams(maxRotationAngle=1)
    assert not mp.is_merge_needed()
    mp = MergeParams(maxDistance=1, maxRotationAngle=1)
    assert mp.is_merge_needed()


def test_SplitParams_is_split_needed():
    sp = SplitParams()
    assert not sp.is_split_needed()
    sp = SplitParams(maxDistance=1)
    assert sp.is_split_needed()
    sp = SplitParams(maxTime=1)
    assert sp.is_split_needed()
    sp = SplitParams(maxDistance=1, maxTime=1)
    assert sp.is_split_needed()


@pytest.mark.parametrize(
    ("lat1", "lon1", "lat2", "lon2", "dist"),
    [
        (0, 0, 0, 0, 0),
        (48.8566, 2.3522, 40.7128, -74.0060, 5837240),
        (51.5007, -0.1246, 51.5014, -0.1419, 1200),
        (-33.865143, 151.209900, 34.052235, -118.243683, 12073263),
    ],
)
def test_Picture_distance_to(lat1, lon1, lat2, lon2, dist):
    p1 = Picture("p1.jpg", GeoPicTags(lat1, lon1, datetime.datetime.now(), None, "equirectangular", None, None, None, None))
    p2 = Picture("p2.jpg", GeoPicTags(lat2, lon2, datetime.datetime.now(), None, "equirectangular", None, None, None, None))
    assert int(p1.distance_to(p2)) == dist
    assert int(p2.distance_to(p1)) == dist


def pic(
    filename: str,
    ts: Optional[datetime.datetime] = datetime.datetime.now(),
    lat: Optional[float] = 0,
    lon: Optional[float] = 0,
) -> Picture:
    return Picture(
        filename,
        GeoPicTags(
            lat=lat or 0,
            lon=lon or 0,
            ts=ts or datetime.datetime.now(),
            heading=None,
            type="equirectangular",
            make=None,
            model=None,
            focal_length=None,
            crop=None,
        ),
    )


@pytest.fixture
def pic1():
    return Picture(
        "p1.jpg",
        GeoPicTags(
            lat=48.8566,
            lon=2.3522,
            ts=datetime.datetime(2024, 1, 1, 12, 0, 0),
            heading=None,
            type="equirectangular",
            make=None,
            model=None,
            focal_length=None,
            crop=None,
        ),
    )


@pytest.fixture
def pic2():
    return Picture(
        "p2.jpg",
        GeoPicTags(
            lat=48.8566,
            lon=2.3522,
            ts=datetime.datetime(2024, 1, 1, 12, 5, 0),
            heading=None,
            type="equirectangular",
            make=None,
            model=None,
            focal_length=None,
            crop=None,
        ),
    )


@pytest.fixture
def pic3():
    return Picture(
        "p3.jpg",
        GeoPicTags(
            lat=40.7128,
            lon=-74.0060,
            ts=datetime.datetime(2024, 1, 1, 12, 10, 0),
            heading=None,
            type="equirectangular",
            make=None,
            model=None,
            focal_length=None,
            crop=None,
        ),
    )


def test_Sequence_from_ts(pic1, pic2):
    seq = Sequence(pictures=[pic1, pic2])
    assert seq.from_ts() == pic1.metadata.ts


def test_Sequence_from_ts_empty():
    seq = Sequence(pictures=[])
    assert seq.from_ts() is None


def test_Sequence_to_ts(pic1, pic2):
    seq = Sequence(pictures=[pic1, pic2])
    assert seq.to_ts() == pic2.metadata.ts


def test_Sequence_to_ts_empty():
    seq = Sequence(pictures=[])
    assert seq.to_ts() is None


def test_Sequence_delta_with(pic1, pic2, pic3):
    seq1 = Sequence(pictures=[pic1, pic2])
    seq2 = Sequence(pictures=[pic3])
    assert seq1.delta_with(seq2) == (datetime.timedelta(minutes=5), 5837240.90382584)


@pytest.mark.parametrize(
    ("data", "method", "expected"),
    (
        (["1.jpg", "2.jpg", "3.jpg"], "filename-asc", ["1.jpg", "2.jpg", "3.jpg"]),
        (["3.jpg", "1.jpg", "2.jpg"], "filename-asc", ["1.jpg", "2.jpg", "3.jpg"]),
        (["3.jpg", "1.jpg", "2.jpeg"], "filename-asc", ["1.jpg", "2.jpeg", "3.jpg"]),
        (["10.jpg", "5.jpg", "1.jpg"], "filename-asc", ["1.jpg", "5.jpg", "10.jpg"]),
        (["C.jpg", "A.jpg", "B.jpg"], "filename-asc", ["A.jpg", "B.jpg", "C.jpg"]),
        (
            ["CAM1_001.jpg", "CAM2_002.jpg", "CAM1_002.jpg"],
            "filename-asc",
            ["CAM1_001.jpg", "CAM1_002.jpg", "CAM2_002.jpg"],
        ),
        (["1.jpg", "2.jpg", "3.jpg"], "filename-desc", ["3.jpg", "2.jpg", "1.jpg"]),
        (["3.jpg", "1.jpg", "2.jpg"], "filename-desc", ["3.jpg", "2.jpg", "1.jpg"]),
        (["3.jpg", "1.jpg", "2.jpeg"], "filename-desc", ["3.jpg", "2.jpeg", "1.jpg"]),
        (["10.jpg", "5.jpg", "1.jpg"], "filename-desc", ["10.jpg", "5.jpg", "1.jpg"]),
        (["C.jpg", "A.jpg", "B.jpg"], "filename-desc", ["C.jpg", "B.jpg", "A.jpg"]),
        (
            ["CAM1_001.jpg", "CAM2_002.jpg", "CAM1_002.jpg"],
            "filename-desc",
            ["CAM2_002.jpg", "CAM1_002.jpg", "CAM1_001.jpg"],
        ),
    ),
)
def test_sort_pictures_names(data, method, expected):
    original_pics = [pic(p) for p in data]
    sorted_pics = sort_pictures(original_pics, method)
    assert expected == [pic.filename for pic in sorted_pics]


@pytest.mark.parametrize(
    ("data", "method", "expected"),
    (
        (
            [["1.jpg", 1], ["2.jpg", 2], ["3.jpg", 3]],
            "time-asc",
            ["1.jpg", "2.jpg", "3.jpg"],
        ),
        (
            [["1.jpg", 2], ["2.jpg", 3], ["3.jpg", 1]],
            "time-asc",
            ["3.jpg", "1.jpg", "2.jpg"],
        ),
        (
            [["1.jpg", 1.01], ["2.jpg", 1.02], ["3.jpg", 1.03]],
            "time-asc",
            ["1.jpg", "2.jpg", "3.jpg"],
        ),
        (
            [["1.jpg", 1], ["2.jpg", 2], ["3.jpg", 3]],
            "time-desc",
            ["3.jpg", "2.jpg", "1.jpg"],
        ),
        (
            [["1.jpg", 2], ["2.jpg", 3], ["3.jpg", 1]],
            "time-desc",
            ["2.jpg", "1.jpg", "3.jpg"],
        ),
        (
            [["1.jpg", 1.01], ["2.jpg", 1.02], ["3.jpg", 1.03]],
            "time-desc",
            ["3.jpg", "2.jpg", "1.jpg"],
        ),
    ),
)
def test_sort_pictures_time(data, method, expected):
    original_pics = [pic(p, datetime.datetime.fromtimestamp(ts)) for (p, ts) in data]
    sorted_pics = sort_pictures(original_pics, method)
    assert expected == [pic.filename for pic in sorted_pics]


def test_find_duplicates():
    def getData():
        return [
            Picture(
                "1.jpg",
                GeoPicTags(
                    lon=12.2,
                    lat=43.4,
                    ts=1516589529,
                    type="flat",
                    model=None,
                    crop=None,
                    focal_length=None,
                    heading=12,
                    make=None,
                ),
            ),
            Picture(
                "2.jpg",
                GeoPicTags(
                    lon=12.2000001,
                    lat=43.4000001,
                    ts=1516589530,
                    type="flat",
                    model=None,
                    crop=None,
                    focal_length=None,
                    heading=12,
                    make=None,
                ),
            ),
            Picture(
                "3.jpg",
                GeoPicTags(
                    lon=12.2000002,
                    lat=43.4000002,
                    ts=1516589531,
                    type="flat",
                    model=None,
                    crop=None,
                    focal_length=None,
                    heading=102,
                    make=None,
                ),
            ),
            Picture(
                "4.jpg",
                GeoPicTags(
                    lon=12.3,
                    lat=43.5,
                    ts=1516589532,
                    type="flat",
                    model=None,
                    crop=None,
                    focal_length=None,
                    heading=102,
                    make=None,
                ),
            ),
        ]

    # Using both dist and angle
    pics = getData()
    dedupPics, dups = find_duplicates(pics, MergeParams(maxDistance=5, maxRotationAngle=10))
    assert dedupPics == [pics[0], pics[2], pics[3]]
    assert dups == [pics[1]]

    # Using only distance
    pics = getData()
    dedupPics, dups = find_duplicates(pics, MergeParams(maxDistance=5))
    assert dedupPics == [pics[0], pics[3]]
    assert dups == [pics[1], pics[2]]

    # Using no param
    pics = getData()
    dedupPics, dups = find_duplicates(pics)
    assert dedupPics == [pics[0], pics[1], pics[2], pics[3]]
    assert dups == []

    # Using only rotation
    pics = getData()
    dedupPics, dups = find_duplicates(pics, MergeParams(maxRotationAngle=5))
    assert dedupPics == [pics[0], pics[1], pics[2], pics[3]]
    assert dups == []


def test_find_duplicates_many_near():
    def getData():
        return [
            Picture(
                "1.jpg",
                GeoPicTags(
                    lon=12.2,
                    lat=43.4,
                    ts=1516589529,
                    type="flat",
                    model=None,
                    crop=None,
                    focal_length=None,
                    heading=12,
                    make=None,
                ),
            ),
            Picture(
                "2.jpg",
                GeoPicTags(
                    lon=12.20001,
                    lat=43.40001,
                    ts=1516589530,
                    type="flat",
                    model=None,
                    crop=None,
                    focal_length=None,
                    heading=12,
                    make=None,
                ),
            ),
            Picture(
                "3.jpg",
                GeoPicTags(
                    lon=12.20002,
                    lat=43.40002,
                    ts=1516589531,
                    type="flat",
                    model=None,
                    crop=None,
                    focal_length=None,
                    heading=102,
                    make=None,
                ),
            ),
            Picture(
                "4.jpg",
                GeoPicTags(
                    lon=12.20003,
                    lat=43.40003,
                    ts=1516589532,
                    type="flat",
                    model=None,
                    crop=None,
                    focal_length=None,
                    heading=102,
                    make=None,
                ),
            ),
            Picture(
                "5.jpg",
                GeoPicTags(
                    lon=12.20004,
                    lat=43.40004,
                    ts=1516589532,
                    type="flat",
                    model=None,
                    crop=None,
                    focal_length=None,
                    heading=102,
                    make=None,
                ),
            ),
        ]

    # 1m
    pics = getData()
    dedupPics, dups = find_duplicates(pics, MergeParams(maxDistance=1))
    assert len(dedupPics) == 5
    assert len(dups) == 0

    # 2m
    pics = getData()
    dedupPics, dups = find_duplicates(pics, MergeParams(maxDistance=2))
    assert dedupPics == [pics[0], pics[2], pics[4]]
    assert dups == [pics[1], pics[3]]


@pytest.mark.parametrize(
    ("pics", "maxTime", "maxDist", "reasons"),
    (
        [  # Single sequence
            [
                {
                    "lat": 48.0000001,
                    "lon": -1.7800001,
                    "ts": 1,
                    "heading": 100,
                    "seq": 0,
                },
                {
                    "lat": 48.0000002,
                    "lon": -1.7800002,
                    "ts": 2,
                    "heading": 100,
                    "seq": 0,
                },
            ],
            None,
            None,
            [],
        ],
        [  # Time excedeed
            [
                {
                    "lat": 48.0000001,
                    "lon": -1.7800001,
                    "ts": 1,
                    "heading": 100,
                    "seq": 0,
                },
                {
                    "lat": 48.0000002,
                    "lon": -1.7800002,
                    "ts": 500,
                    "heading": 100,
                    "seq": 1,
                },
            ],
            10,
            None,
            ["time"],
        ],
        [  # Time excedeed, reverse
            [
                {
                    "lat": 48.0000001,
                    "lon": -1.7800001,
                    "ts": 500,
                    "heading": 100,
                    "seq": 0,
                },
                {
                    "lat": 48.0000002,
                    "lon": -1.7800002,
                    "ts": 1,
                    "heading": 100,
                    "seq": 1,
                },
            ],
            10,
            None,
            ["time"],
        ],
        [  # Distance excedeed
            [
                {
                    "lat": 48.0000001,
                    "lon": -1.7800001,
                    "ts": 1,
                    "heading": 100,
                    "seq": 0,
                },
                {
                    "lat": 48.1000000,
                    "lon": -1.7800002,
                    "ts": 2,
                    "heading": 100,
                    "seq": 1,
                },
            ],
            None,
            1,
            ["distance"],
        ],
        [  # Many sequences
            [
                {
                    "lat": 48.0000001,
                    "lon": -1.7800001,
                    "ts": 1,
                    "heading": 100,
                    "seq": 0,
                },
                {
                    "lat": 48.0000002,
                    "lon": -1.7800002,
                    "ts": 2,
                    "heading": 100,
                    "seq": 0,
                },
                {
                    "lat": 48.0000003,
                    "lon": -1.7800003,
                    "ts": 3,
                    "heading": 100,
                    "seq": 0,
                },
                # Distance excedeed
                {
                    "lat": 48.1000000,
                    "lon": -1.7800001,
                    "ts": 4,
                    "heading": 100,
                    "seq": 1,
                },
                {
                    "lat": 48.1000001,
                    "lon": -1.7800001,
                    "ts": 5,
                    "heading": 100,
                    "seq": 1,
                },
                {
                    "lat": 48.1000002,
                    "lon": -1.7800001,
                    "ts": 6,
                    "heading": 100,
                    "seq": 1,
                },
                # Time excedeed
                {
                    "lat": 48.1000003,
                    "lon": -1.7800001,
                    "ts": 100,
                    "heading": 100,
                    "seq": 2,
                },
                {
                    "lat": 48.1000004,
                    "lon": -1.7800001,
                    "ts": 101,
                    "heading": 100,
                    "seq": 2,
                },
                {
                    "lat": 48.1000005,
                    "lon": -1.7800001,
                    "ts": 102,
                    "heading": 100,
                    "seq": 2,
                },
            ],
            30,
            100,
            ["distance", "time"],
        ],
    ),
)
def test_split_in_sequences(pics, maxTime, maxDist, reasons):
    sp = SplitParams(maxDistance=maxDist, maxTime=maxTime)
    inputPics = []
    expectedPics = [[]]

    for id, pic in enumerate(pics):
        inputPics.append(
            Picture(
                f"{id}.jpg",
                GeoPicTags(
                    lat=pic["lat"],
                    lon=pic["lon"],
                    ts=datetime.datetime.fromtimestamp(pic["ts"]),
                    heading=pic["heading"],
                    type="equirectangular",
                    make=None,
                    model=None,
                    focal_length=None,
                    crop=None,
                ),
            )
        )

        if len(expectedPics) - 1 < pic["seq"]:
            expectedPics.append([])
        expectedPics[pic["seq"]].append(f"{id}.jpg")

    seqs, splits = split_in_sequences(inputPics, sp)
    print("Got     ", [[p.filename for p in r.pictures] for r in seqs])
    print("Expected", expectedPics)
    assert len(seqs) == len(expectedPics)
    assert len(splits) == len(expectedPics) - 1

    for i, resSubSeq in enumerate(seqs):
        print("Checking sequence", i)
        assert len(resSubSeq.pictures) == len(expectedPics[i])
        for j, resSubSeqPic in enumerate(resSubSeq.pictures):
            print(" -> Checking pic", j)
            assert resSubSeqPic.filename == expectedPics[i][j]

    # Check splits
    for i, resSplit in enumerate(splits):
        assert resSplit.prevPic == seqs[i].pictures[-1]
        assert resSplit.nextPic == seqs[i + 1].pictures[0]
        assert resSplit.reason == reasons[i]


def test_dispatch_pictures():
    pic1 = pic("img1.jpg", datetime.datetime(2023, 1, 1, 12, 0, 0), 48.8566, 2.3522)
    pic2 = pic("img2.jpg", datetime.datetime(2023, 1, 1, 12, 0, 2), 48.8570, 2.3530)
    # Time split
    pic3 = pic("img3.jpg", datetime.datetime(2023, 1, 1, 12, 10, 0), 48.8566, 2.3522)
    pic4 = pic("img4.jpg", datetime.datetime(2023, 1, 1, 12, 10, 2), 48.8566, 2.3522)  # Dup of pic3

    report = dispatch_pictures([pic1, pic2, pic3, pic4], SortMethod.time_asc, MergeParams(maxDistance=5.0), SplitParams(maxTime=300))

    assert len(report.sequences) == 2
    assert len(report.sequences[0].pictures) == 2
    assert len(report.sequences[1].pictures) == 1
    assert len(report.duplicate_pictures) == 1
    assert len(report.sequences_splits) == 1

    assert report.duplicate_pictures[0] == pic4

    split = report.sequences_splits[0]
    assert split.prevPic == pic2
    assert split.nextPic == pic3
    assert split.reason == SplitReason.time
