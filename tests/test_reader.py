import os
import pytest
from fractions import Fraction
from geopic_tag_reader import reader
from .conftest import FIXTURE_DIR, openImg


def assertGeoPicTagsEquals(gpt, expectedDict):
    assert gpt.lat == expectedDict.get("lat")
    assert gpt.lon == expectedDict.get("lon")
    assert gpt.heading == expectedDict.get("heading")
    assert gpt.type == expectedDict.get("type")
    assert gpt.make == expectedDict.get("make")
    assert gpt.model == expectedDict.get("model")
    assert gpt.focal_length == expectedDict.get("focal_length")
    assert gpt.altitude == expectedDict.get("altitude")
    assert gpt.pitch == expectedDict.get("pitch")
    assert gpt.roll == expectedDict.get("roll")
    assert gpt.yaw == expectedDict.get("yaw")
    assert gpt.sensor_width == expectedDict.get("sensor_width")
    assert gpt.gps_accuracy == expectedDict.get("gps_accuracy")
    assert gpt.field_of_view == expectedDict.get("field_of_view")
    assert gpt.tagreader_warnings == expectedDict.get("tagreader_warnings", [])
    assert len(gpt.exif) > 0

    if expectedDict.get("ts") is not None:
        assert gpt.ts is not None
        assert gpt.ts.isoformat() == expectedDict["ts"]
    else:
        assert gpt.ts is None

    if gpt.crop:
        assert expectedDict.get("crop") is not None
        assert gpt.crop.fullWidth == expectedDict["crop"].get("fullWidth")
        assert gpt.crop.fullHeight == expectedDict["crop"].get("fullHeight")
        assert gpt.crop.width == expectedDict["crop"].get("width")
        assert gpt.crop.height == expectedDict["crop"].get("height")
        assert gpt.crop.left == expectedDict["crop"].get("left")
        assert gpt.crop.top == expectedDict["crop"].get("top")
    else:
        assert expectedDict.get("crop") is None

    if expectedDict.get("ts_by_source"):
        if expectedDict["ts_by_source"]["gps"] is None:
            assert gpt.ts_by_source.gps is None
        else:
            assert expectedDict["ts_by_source"]["gps"] == gpt.ts_by_source.gps.isoformat()
        if expectedDict["ts_by_source"]["camera"] is None:
            assert gpt.ts_by_source.camera is None
        else:
            assert expectedDict["ts_by_source"]["camera"] == gpt.ts_by_source.camera.isoformat()
    else:
        assert gpt.ts_by_source is None


def test_decodeFloat_with_float():
    data = {"Tag1": 12.345}
    tags = ["Tag1"]
    result = reader.decodeFloat(data, tags)
    assert result == 12.345


def test_decodeFloat_with_fraction():
    data = {"Tag1": Fraction(22, 7)}
    tags = ["Tag1"]
    result = reader.decodeFloat(data, tags)
    assert result == pytest.approx(22 / 7)


def test_decodeFloat_with_precision():
    data = {"Tag1": 12.3456789}
    tags = ["Tag1"]
    result = reader.decodeFloat(data, tags, precision=2)
    assert result == 12.35


def test_decodeFloat_with_invalid_tag():
    data = {"Tag1": 12.345}
    tags = ["InvalidTag"]
    result = reader.decodeFloat(data, tags)
    assert result is None


def test_decodeFloat_with_multiple_tags():
    data = {"Tag1": 12.345, "Tag2": Fraction(22, 7)}
    tags = ["Tag2", "Tag1"]
    result = reader.decodeFloat(data, tags)
    assert result == pytest.approx(22 / 7)


def test_decodeFloat_with_no_tags():
    data = {"Tag1": 12.345}
    tags = []
    result = reader.decodeFloat(data, tags)
    assert result is None


def test_decodeFloat_with_non_usable_tag():
    data = {"Tag1": "NotAFloat"}
    tags = ["Tag1"]
    result = reader.decodeFloat(data, tags)
    assert result is None


def test_decodeFloat_with_precision_and_fraction():
    data = {"Tag1": Fraction(22, 7)}
    tags = ["Tag1"]
    result = reader.decodeFloat(data, tags, precision=2)
    assert result == 3.14


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "1.jpg"))
def test_readPictureMetadata(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/1.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "lat": 49.00688961988304,
            "lon": 1.9191854417991367,
            "ts": "2021-07-29T11:16:54+02:00",
            "heading": 349,
            "type": "equirectangular",
            "make": "GoPro",
            "model": "Max",
            "focal_length": 3,
            "altitude": 93,
            "roll": 0,
            "pitch": 0,
            "yaw": 0,
            "sensor_width": 6.17,
            "gps_accuracy": 4,
            "field_of_view": 360,
            "ts_by_source": {"camera": "2021-07-29T11:16:42+02:00", "gps": "2021-07-29T11:16:54+02:00"},
            "tagreader_warnings": ["No GPS horizontal positioning error value found, GPS accuracy can only be estimated"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "a1.jpg"))
def test_readPictureMetadata_negCoords(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/a1.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "lat": 48.33756428166505,
            "lon": -1.9331088333333333,
            "ts": "2022-05-13T16:53:00+02:00",
            "heading": 32,
            "type": "equirectangular",
            "make": "GoPro",
            "model": "Max",
            "focal_length": 3,
            "altitude": 79,
            "roll": 0,
            "pitch": 0,
            "yaw": 0,
            "sensor_width": 6.17,
            "gps_accuracy": 4,
            "field_of_view": 360,
            "ts_by_source": {"camera": "2022-05-13T16:54:11+02:00", "gps": "2022-05-13T16:53:00+02:00"},
            "tagreader_warnings": ["No GPS horizontal positioning error value found, GPS accuracy can only be estimated"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "b1.jpg"))
def test_readPictureMetadata_flat(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/b1.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "lat": 48.139852239480945,
            "lon": -1.9499731060073981,
            "ts": "2015-04-25T15:37:48+02:00",
            "heading": 155,
            "type": "flat",
            "make": "OLYMPUS IMAGING CORP.",
            "model": "SP-720UZ",
            "focal_length": 4.66,
            "sensor_width": 6.16,
            "gps_accuracy": None,
            "field_of_view": 67,
            "ts_by_source": {"camera": "2015-04-25T15:37:48+02:00", "gps": None},
            "tagreader_warnings": ["No GPS accuracy value found, this prevents computing a quality score"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "c1.jpg"))
def test_readPictureMetadata_flat2(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/c1.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "lat": 48.85779642035038,
            "lon": 2.3392783047650747,
            "ts": "2015-05-04T13:08:52+02:00",
            "heading": 302,
            "type": "flat",
            "make": "Canon",
            "model": "EOS 6D0",
            "focal_length": 35.0,
            "sensor_width": 35.8,
            "gps_accuracy": None,
            "field_of_view": 54,
            "ts_by_source": {"camera": "2015-05-04T13:08:52+02:00", "gps": None},
            "tagreader_warnings": ["No GPS accuracy value found, this prevents computing a quality score"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "d1.jpg"))
def test_readPictureMetadata_xmpHeading(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/d1.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "lat": 50.87070833333333,
            "lon": -1.5260916666666666,
            "ts": "2020-09-13T15:45:41.767000+01:00",
            "heading": None,
            "type": "equirectangular",
            "make": "Google",
            "model": "Pixel 3",
            "focal_length": None,
            "altitude": 68,
            "yaw": 67,
            "sensor_width": None,
            "gps_accuracy": 5,
            "field_of_view": 360,
            "ts_by_source": {"camera": "2020-09-13T15:45:41.767000+01:00", "gps": "2020-09-13T15:40:19+01:00"},
            "tagreader_warnings": [
                "No heading value was found, this reduces usability of picture",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "e1.jpg"))
def test_readPictureMetadata_noHeading(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/e1.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "lat": 48.15506638888889,
            "lon": -1.6844680555555556,
            "ts": "2022-10-19T09:56:34+02:00",
            "heading": None,
            "type": "flat",
            "make": "SONY",
            "model": "FDR-X1000V",
            "focal_length": 2.8,
            "altitude": 34,
            "sensor_width": 6.19,
            "gps_accuracy": 3.36,
            "field_of_view": 96,
            "ts_by_source": {"camera": "2022-10-19T08:56:34+02:00", "gps": "2022-10-19T09:56:34+02:00"},
            "tagreader_warnings": [
                "No heading value was found, this reduces usability of picture",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_Ricoh_Theta.jpg"))
def test_readPictureMetadata_ricoh_theta(datafiles):
    for f in datafiles.iterdir():
        result = reader.readPictureMetadata(openImg(str(f)))
        assertGeoPicTagsEquals(
            result,
            {
                "focal_length": 0.75,
                "heading": None,
                "lat": 48.83930905577957,
                "lon": 2.3205357914890987,
                "make": "RICOH",
                "model": "THETA m15",
                "ts": "2016-03-25T14:12:13+01:00",
                "type": "equirectangular",
                "roll": 3,
                "pitch": 1,
                "yaw": 270,
                "sensor_width": None,
                "gps_accuracy": 5,
                "field_of_view": 360,
                "ts_by_source": {"camera": "2016-03-25T14:12:13+01:00", "gps": "2016-03-25T14:12:13+01:00"},
                "tagreader_warnings": [
                    "No heading value was found, this reduces usability of picture",
                    "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
                ],
            },
        )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_V4MPack.jpg"))
def test_readPictureMetadata_v4mpack(datafiles):
    for f in datafiles.iterdir():
        result = reader.readPictureMetadata(openImg(str(f)))
        assertGeoPicTagsEquals(
            result,
            {
                "focal_length": None,
                "heading": 64,
                "lat": 47.08506017299737,
                "lon": -1.2761512389983616,
                "make": "STFMANI",
                "model": "V4MPOD 1",
                "ts": "2019-04-16T14:20:13+02:00",
                "type": "equirectangular",
                "altitude": 34,
                "sensor_width": None,
                "gps_accuracy": 2,
                "field_of_view": 360,
                "ts_by_source": {"camera": "2019-04-16T14:20:13+02:00", "gps": "2019-04-16T14:20:13+02:00"},
                "tagreader_warnings": [
                    "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
                ],
            },
        )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "IMG_20210720_161352.jpg"))
def test_readPictureMetadata_a5000(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/IMG_20210720_161352.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "focal_length": 4.103,
            "heading": 355,
            "lat": 48.96280504578332,
            "lon": 2.51197323068765,
            "make": "OnePlus",
            "model": "ONEPLUS A5000",
            "ts": "2021-07-20T16:13:52.199995+02:00",
            "type": "flat",
            "altitude": 0,
            "sensor_width": None,
            "gps_accuracy": 5,
            "field_of_view": 74,
            "ts_by_source": {"camera": "2021-07-20T16:13:52.199995+02:00", "gps": None},
            "tagreader_warnings": ["No GPS horizontal positioning error value found, GPS accuracy can only be estimated"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "IMG_20210720_144918.jpg"))
def test_readPictureMetadata_a5000_2(datafiles):
    with pytest.raises(reader.PartialExifException) as e_info:
        result = reader.readPictureMetadata(openImg(str(datafiles) + "/IMG_20210720_144918.jpg"))


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_V4MPack.jpg"))
def test_readPictureMetadata_comment(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/img_V4MPack.jpg"))
    assert result.exif.get("Exif.Photo.UserComment") == "DCIM\\627MEDIA"


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "int_long_tag.jpg"))
def test_readPictureMetadata_int_long_tag(datafiles):
    """
    int_long_tag.jpg has an invalid longituderef tag, it's '52227'
    But since the  longitude ref is optional (and default to E based on https://www.exiftool.org/geotag.html) we get a valid position
    """
    r = reader.readPictureMetadata(openImg(str(datafiles) + "/int_long_tag.jpg"))
    assertGeoPicTagsEquals(
        r,
        {
            "focal_length": None,
            "heading": 255,
            "lat": 44.09732280555556,
            "lon": 4.700622,
            "make": None,
            "model": None,
            "ts": "2023-01-12T09:17:00+01:00",
            "type": "flat",
            "sensor_width": None,
            "gps_accuracy": None,
            "field_of_view": None,
            "tagreader_warnings": [
                "GPSLongitudeRef not found, assuming GPSLongitudeRef is East",
                "No make and model value found, no assumption on focal length or GPS precision can be made",
                "No focal length value was found, this prevents calculating field of view",
                "No GPS accuracy value found, this prevents computing a quality score",
            ],
            "ts_by_source": {"camera": "2023-01-12T09:17:00+01:00", "gps": None},
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "int_long_tag.jpg"))
def test_readPictureMetadata_i18n(datafiles):
    r = reader.readPictureMetadata(openImg(str(datafiles) + "/int_long_tag.jpg"), "fr")
    print(r.tagreader_warnings)
    assertGeoPicTagsEquals(
        r,
        {
            "focal_length": None,
            "heading": 255,
            "lat": 44.09732280555556,
            "lon": 4.700622,
            "make": None,
            "model": None,
            "ts": "2023-01-12T09:17:00+01:00",
            "type": "flat",
            "tagreader_warnings": [
                "GPSLongitudeRef non-trouvé, utilisation de l'Est par défaut",
                "Aucune info sur le modèle de la caméra (make/model), aucune supposition ne peut être faite sur la longueur focale ou la précision du GPS",
                "Aucune valeur pour la longueur focale (focal length) trouvée, cela empêche de calculer l'angle de vue horizontal",
                "Aucune valeur trouvée pour la précision du GPS, cela empêche de calculer le score de qualité",
            ],
            "ts_by_source": {"camera": "2023-01-12T09:17:00+01:00", "gps": None},
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_invalid_gps_date.jpg"))
def test_readPictureMetadata_invalidGpsDate(datafiles):
    """
    Handles fallback to OriginalDate EXIF tag if GPS time is invalid
    """
    r = reader.readPictureMetadata(openImg(str(datafiles) + "/img_invalid_gps_date.jpg"))
    assertGeoPicTagsEquals(
        r,
        {
            "focal_length": 4.2,
            "heading": 202,
            "lat": 48.88026828330809,
            "lon": 2.358506155368721,
            "make": "samsung",
            "model": None,
            "ts": "2021-09-08T11:43:57.075400+02:00",
            "type": "flat",
            "altitude": 73,
            "sensor_width": None,
            "gps_accuracy": 5,
            "field_of_view": 69,
            "ts_by_source": {"camera": "2021-09-08T11:43:57.075400+02:00", "gps": None},
            "tagreader_warnings": [
                "Skipping GPS date/time (Exif.GPSInfo group) as it was not recognized: 2021:09:08H",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_gps_datestamp.jpg"))
def test_readPictureMetadata_gpsDateStamp(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/img_gps_datestamp.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "focal_length": 4.2,
            "heading": 138,
            "lat": 48.80334166666666,
            "lon": 2.4833194444444446,
            "make": "Apple",
            "model": "iPhone 12 Pro",
            "ts": "2023-04-29T18:30:51.003000+02:00",
            "type": "flat",
            "altitude": 36,
            "sensor_width": None,
            "gps_accuracy": 42,
            "field_of_view": 69,
            "ts_by_source": {"camera": "2023-04-29T16:30:51.565000+02:00", "gps": "2023-04-29T18:30:51.003000+02:00"},
            "tagreader_warnings": ["No GPS horizontal positioning error value found, GPS accuracy can only be estimated"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_gps_date_string.jpg"))
def test_readPictureMetadata_gpsDateString(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/img_gps_date_string.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "focal_length": 3.99,
            "heading": 182,
            "lat": 45.624741666666665,
            "lon": -1.0015555555555555,
            "make": "Apple",
            "model": "iPhone 8 Plus",
            "ts": "2019-07-28T13:25:42.529000+02:00",
            "type": "flat",
            "altitude": 19,
            "sensor_width": None,
            "gps_accuracy": 12,
            "field_of_view": 65,
            "ts_by_source": {"camera": None, "gps": "2019-07-28T13:25:42.529000+02:00"},
            "tagreader_warnings": ["No GPS horizontal positioning error value found, GPS accuracy can only be estimated"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_categorisee.jpg"))
def test_readPictureMetadata_categories(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/img_categorisee.jpg"))
    assert "Xmp.mediapro.CatalogSets" in result.exif
    assert "Xmp.MicrosoftPhoto.LastKeywordXMP" in result.exif
    assert "Xmp.acdsee.categories" in result.exif


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "cropped.jpg"))
def test_readPictureMetadata_cropped(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/cropped.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "heading": 349,
            "lat": 49.00688961988304,
            "lon": 1.919185441804927,
            "make": "GoPro",
            "model": "Max",
            "ts": "2021-07-29T11:16:54+02:00",
            "type": "equirectangular",
            "altitude": 93,
            "crop": {"fullWidth": 4032, "fullHeight": 2016, "width": 2150, "height": 1412, "top": 134, "left": 538},
            "sensor_width": 6.17,
            "gps_accuracy": 4,
            "field_of_view": 360,
            "ts_by_source": {"camera": "2021-07-29T11:16:42+02:00", "gps": "2021-07-29T11:16:54+02:00"},
            "tagreader_warnings": [
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_datetimeoriginal.jpg"))
def test_readPictureMetadata_datetimeoriginal(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/img_datetimeoriginal.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "heading": 228,
            "lat": 47.05369676823944,
            "lon": -1.382302762883527,
            "make": "Motorola",
            "model": "XT1052",
            "ts": "2020-08-31T09:36:28+02:00",
            "type": "flat",
            "altitude": 72,
            "gps_accuracy": 5,
            "ts_by_source": {"camera": "2020-08-31T09:36:28+02:00", "gps": None},
            "tagreader_warnings": [
                "GPSTimeStamp and GPSDateTime don't contain supported time format (in Exif.GPSInfo group)",
                "No focal length value was found, this prevents calculating field of view",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "datetime_ms_float.jpg"))
def test_readPictureMetadata_datetimeoriginal_decimal_milliseconds(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/datetime_ms_float.jpg"))
    assertGeoPicTagsEquals(
        result,
        {
            "heading": 62,
            "lat": 14.527860666666667,
            "lon": 121.15072633333334,
            "make": "BlackVue",
            "model": "DR900S-1CH",
            "ts": "2023-11-12T00:25:19.023000+08:00",
            "type": "flat",
            "altitude": 29,
            "gps_accuracy": 4,
            "ts_by_source": {"camera": "2023-11-12T00:25:19.023000+08:00", "gps": None},
            "tagreader_warnings": [
                "No focal length value was found, this prevents calculating field of view",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_without_exif_tags.jpg"))
def test_readPictureWithoutExifTags(datafiles):
    """Reading tags from a picture that does not contains any should result of an exception with information about the missing tags"""
    with pytest.raises(reader.PartialExifException) as e_info:
        reader.readPictureMetadata(openImg(str(datafiles) + "/img_without_exif_tags.jpg"))

    e = e_info.value
    assert e.missing_mandatory_tags == {"lon", "lat", "datetime"}
    assert (
        str(e)
        == """The picture is missing mandatory metadata:
	- No GPS coordinates or broken coordinates in picture EXIF tags
	- No valid date in picture EXIF tags"""
    )
    assert e.tags == reader.PartialGeoPicTags(
        type="flat",
        tagreader_warnings=[
            "No heading value was found, this reduces usability of picture",
            "No make and model value found, no assumption on focal length or GPS precision can be made",
            "No focal length value was found, this prevents calculating field of view",
            "No GPS accuracy value found, this prevents computing a quality score",
        ],
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_without_coord.jpg"))
def test_readPictureWithoutCoord(datafiles):
    """Reading tags from a picture that does not contains a coordinate should result of an exception"""
    with pytest.raises(reader.PartialExifException) as e_info:
        reader.readPictureMetadata(openImg(str(datafiles) + "/img_without_coord.jpg"))

    e = e_info.value
    assert e.missing_mandatory_tags == {"lat", "lon"}
    assert str(e) == "No GPS coordinates or broken coordinates in picture EXIF tags"
    # all the information possible should have been read
    assertGeoPicTagsEquals(
        e.tags,
        {
            "focal_length": 4.2,
            "heading": 138,
            "lat": None,  # but lat/lon should be None
            "lon": None,
            "make": "Apple",
            "model": "iPhone 12 Pro",
            "ts": "2023-04-29T16:30:51.003000+00:00",
            "type": "flat",
            "altitude": 36,
            "sensor_width": None,
            "gps_accuracy": 42,
            "field_of_view": 69,
            "ts_by_source": {"camera": "2023-04-29T16:30:51.565000+02:00", "gps": "2023-04-29T16:30:51.003000+00:00"},
            "tagreader_warnings": ["No GPS horizontal positioning error value found, GPS accuracy can only be estimated"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "img_without_dt.jpg"))
def test_readPictureWithoutDatetime(datafiles):
    """Reading tags from a picture that does not contains a datetime should result of an exception"""
    with pytest.raises(reader.PartialExifException) as e_info:
        reader.readPictureMetadata(openImg(str(datafiles) + "/img_without_dt.jpg"))

    e = e_info.value
    assert e.missing_mandatory_tags == {"datetime"}
    assert str(e) == "No valid date in picture EXIF tags"

    # all the information possible should have been read
    assertGeoPicTagsEquals(
        e.tags,
        {
            "focal_length": 4.2,
            "heading": 138,
            "lat": 48.80334166666666,
            "lon": 2.4833194444444446,
            "make": "Apple",
            "model": "iPhone 12 Pro",
            "ts": None,  # No datetime should have been parsed
            "type": "flat",
            "altitude": 36,
            "sensor_width": None,
            "gps_accuracy": 42,
            "field_of_view": 69,
            "tagreader_warnings": [
                "GPSTimeStamp and GPSDateTime don't contain supported time format (in Xmp.exif group)",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "out_of_bounds_lat.jpg"))
def test_readPictureMetadata_invalidLat(datafiles):
    with pytest.raises(reader.InvalidExifException) as e_info:
        reader.readPictureMetadata(openImg(str(datafiles) + "/out_of_bounds_lat.jpg"))

    e = e_info.value
    assert str(e) == "Read latitude is out of WGS84 bounds (should be in [-90, 90])"


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "out_of_bounds_lon.jpg"))
def test_readPictureMetadata_invalidLon(datafiles):
    with pytest.raises(reader.InvalidExifException) as e_info:
        reader.readPictureMetadata(openImg(str(datafiles) + "/out_of_bounds_lon.jpg"))

    e = e_info.value
    assert str(e) == "Read longitude is out of WGS84 bounds (should be in [-180, 180])"


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "pic_with_float_lat.jpg"))
def test_readPictureWithLatitudeAsFloat(datafiles):
    """Reading a pic with the latitude as float should work as before"""
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/pic_with_float_lat.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "heading": 6,
            "lat": 48.553668,
            "lon": 7.683081,
            "make": None,
            "model": "PULSAR",
            "ts": "2021-11-16T16:18:16.890000+01:00",
            "type": "equirectangular",
            "altitude": None,
            "sensor_width": None,
            "gps_accuracy": None,
            "field_of_view": 360,
            "ts_by_source": {"camera": None, "gps": "2021-11-16T16:18:16.890000+01:00"},
            "tagreader_warnings": [
                "No GPS accuracy value found, this prevents computing a quality score",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "gps_date_time_stamp.jpg"))
def test_readPictureMetadata_gps_date_time_stamp(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/gps_date_time_stamp.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "heading": 6,
            "lat": 48.553668,
            "lon": 7.683081,
            "make": None,
            "model": "PULSAR",
            "ts": "2021-11-16T16:18:16+01:00",
            "type": "equirectangular",
            "altitude": None,
            "sensor_width": None,
            "gps_accuracy": None,
            "field_of_view": 360,
            "ts_by_source": {"camera": None, "gps": "2021-11-16T16:18:16+01:00"},
            "tagreader_warnings": [
                "No GPS accuracy value found, this prevents computing a quality score",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "insta360_date.jpg"))
def test_readPictureMetadata_insta360(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/insta360_date.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "heading": None,
            "lat": 43.29976944444444,
            "lon": 3.482755555555556,
            "make": "Insta360",
            "model": "One X2.PHOTO_NORMAL",
            "ts": "2023-11-22T14:17:49+01:00",
            "type": "equirectangular",
            "altitude": 84,
            "sensor_width": None,
            "gps_accuracy": 4,
            "field_of_view": 360,
            "ts_by_source": {"camera": "2023-11-22T14:17:49+01:00", "gps": None},
            "tagreader_warnings": [
                "No heading value was found, this reduces usability of picture",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "ricoh_theta_no_projection.jpg"))
def test_readPictureMetadata_ricoh_noproj(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/ricoh_theta_no_projection.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "heading": None,
            "lat": 48.23105138608521,
            "lon": -1.5464416503906249,
            "make": "Ricoh",
            "model": "Theta S",
            "ts": "2023-01-01T13:15:42+01:00",
            "type": "equirectangular",
            "altitude": None,
            "sensor_width": None,
            "gps_accuracy": 5,
            "field_of_view": 360,
            "ts_by_source": {"camera": None, "gps": "2023-01-01T13:15:42+01:00"},
            "tagreader_warnings": [
                "No heading value was found, this reduces usability of picture",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "gopromax_flat.jpg"))
def test_readPictureMetadata_gopromax_flat(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/gopromax_flat.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "lat": 47.22555109997222,
            "lon": -1.5631604999722222,
            "ts": "2024-02-17T13:46:32+01:00",
            "heading": None,
            "type": "flat",
            "make": "GoPro",
            "model": "Max",
            "focal_length": 3,
            "altitude": 78,
            "sensor_width": 6.17,
            "gps_accuracy": 4,
            "field_of_view": 92,
            "ts_by_source": {"camera": "2024-02-17T13:46:37+01:00", "gps": "2024-02-17T13:46:32+01:00"},
            "tagreader_warnings": [
                "No heading value was found, this reduces usability of picture",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "gopromax_flat2.jpg"))
def test_readPictureMetadata_gopromax_flat2(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/gopromax_flat2.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "lat": 49.198576799972216,
            "lon": -0.3590469,
            "ts": "2024-09-29T19:10:00.293000+02:00",
            "heading": 78,
            "type": "flat",
            "make": "GoPro",
            "model": "Max",
            "focal_length": None,
            "altitude": 63,
            "sensor_width": 6.17,
            "gps_accuracy": 4,
            "field_of_view": None,
            "ts_by_source": {"camera": "2024-09-29T17:10:00.293000+00:00", "gps": "2024-09-29T19:10:00.293000+02:00"},
            "tagreader_warnings": [
                "No focal length value was found, this prevents calculating field of view",
                "No GPS horizontal positioning error value found, GPS accuracy can only be estimated",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "broken_makernotes.jpg"))
def test_readPictureMetadata_broken_makernotes(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/broken_makernotes.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "heading": 33,
            "lat": 48.24849805555556,
            "lon": -1.7841980555555554,
            "make": "SONY",
            "model": "FDR-X1000V",
            "ts": "2020-09-03T08:50:20+02:00",
            "type": "flat",
            "altitude": 99,
            "focal_length": 2.8,
            "sensor_width": 6.19,
            "gps_accuracy": 4.95,
            "field_of_view": 96,
            "ts_by_source": {"camera": "2020-09-03T07:50:20+02:00", "gps": "2020-09-03T08:50:20+02:00"},
            "tagreader_warnings": ["No GPS horizontal positioning error value found, GPS accuracy can only be estimated"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "datetime_offset.jpg"))
def test_readPictureMetadata_datetime_offset(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/datetime_offset.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "lat": 48.33756428166505,
            "lon": -1.9331088333333333,
            "ts": "2022-05-13T16:54:11+02:00",
            "heading": 32,
            "type": "equirectangular",
            "make": "GoPro",
            "model": "Max",
            "focal_length": 3,
            "altitude": 79,
            "roll": 0,
            "pitch": 0,
            "yaw": 0,
            "sensor_width": 6.17,
            "gps_accuracy": 4,
            "field_of_view": 360,
            "ts_by_source": {"camera": "2022-05-13T16:54:11+02:00", "gps": None},
            "tagreader_warnings": ["No GPS horizontal positioning error value found, GPS accuracy can only be estimated"],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "gps_date_slash.jpg"))
def test_readPictureMetadata_gps_date_slash(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/gps_date_slash.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "lat": 43.42541569992266,
            "lon": 1.3766216000638112,
            "ts": "2023-06-13T09:15:00+02:00",
            "heading": None,
            "type": "flat",
            "make": None,
            "model": None,
            "focal_length": None,
            "altitude": None,
            "ts_by_source": {"camera": "2023-06-13T07:15:00+02:00", "gps": "2023-06-13T09:15:00+02:00"},
            "tagreader_warnings": [
                "GPSLatitudeRef not found, assuming GPSLatitudeRef is North",
                "GPSLongitudeRef not found, assuming GPSLongitudeRef is East",
                "No heading value was found, this reduces usability of picture",
                "No make and model value found, no assumption on focal length or GPS precision can be made",
                "No focal length value was found, this prevents calculating field of view",
                "No GPS accuracy value found, this prevents computing a quality score",
            ],
        },
    )


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "charset.jpg"))
def test_readPictureMetadata_charset(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/charset.jpg"))
    assert result.exif["Exif.Photo.UserComment"] == "CD31 31_D0003_51_600"


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "zero_lat.jpg"))
def test_readPictureMetadata_lat_at_zero(datafiles):
    """it's valid to have the lat at 0"""
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/zero_lat.jpg"))
    assert result.lat == 0
    assert result.lon == 1.9191854417991367


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "zero_lat_lon.jpg"))
def test_readPictureMetadata_lon_lat_at_zero(datafiles):
    """it's not valid to have the lat and lon at 0, it's likely a GPS problem, we don't want null island pictures"""
    with pytest.raises(reader.PartialExifException) as e_info:
        reader.readPictureMetadata(openImg(str(datafiles) + "/zero_lat_lon.jpg"))

    e = e_info.value
    assert str(e) == "No GPS coordinates or broken coordinates in picture EXIF tags"


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, "gpsdop_fraction.jpg"))
def test_readPictureMetadata_gpsdop_fraction(datafiles):
    result = reader.readPictureMetadata(openImg(str(datafiles) + "/gpsdop_fraction.jpg"))

    assertGeoPicTagsEquals(
        result,
        {
            "lat": 47.27363,
            "lon": -2.3242679999999996,
            "ts": "2016-10-20T12:17:18+02:00",
            "heading": 205,
            "type": "equirectangular",
            "make": "FLIR Systems Inc.",
            "model": "Ladybug",
            "altitude": 6,
            "gps_accuracy": 0.9,
            "field_of_view": 360,
            "ts_by_source": {"camera": "2016-10-20T10:17:18+02:00", "gps": "2016-10-20T12:17:18+02:00"},
            "tagreader_warnings": [],
        },
    )
