# Tag Reader's API Reference

## Reader

::: geopic_tag_reader.reader

## Writer

!!! warning

    To use this module, you need to install the `write-exif` dependency:

    ```bash
    pip install -e .[write-exif]
    ```

::: geopic_tag_reader.writer

## Sequence

::: geopic_tag_reader.sequence

## Camera

::: geopic_tag_reader.camera

## Model

::: geopic_tag_reader.model
